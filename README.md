# README #

This is a proof of concept for an abstraction library for Microsoft Chat Bots that will allow a developer to easily integrate chat service into their applications without needing to worry about networking and direct url calls. Some support for reconnecting to existing sessions (this needs to be reviewed).

### Quick integration overview ###

* Grab the ConversationManager instance: ConversationManager.getInstance(this).resumeOrCreateConversation(CHAT_BOT_NAME, secret, callback);
* Check for new messages on a timer: conversation.getMessages(callback);
* Send the chatbot a message: conversation.sendMessage(text, callback);

See MainActivity.java for more details

### > git blame ###

* Will Blaschko (will.blaschko@gmail.com)