package com.willblaschko.android.luisexample;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.willblaschko.android.microsoftchatbot.models.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by willb_000 on 8/25/2016.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MessageHolder> {

    List<Message> messages = new ArrayList<>();
    HashMap<Long, Boolean> contains = new HashMap<>();



    public ChatAdapter(){

    }

    public void clearItems(){
        messages.clear();
        contains.clear();
    }

    @MainThread
    public void addItems(@NonNull List<Message> items){
        if(items == null){
            return;
        }
        final int oldSize = messages.size();
        for(Message message: items){
            if(contains.containsKey(message.getId())){
                continue;
            }
            contains.put(message.getId(), true);
            messages.add(message);
        }
        final int newSize = messages.size();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MessageHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false));
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
        Message message = messages.get(position);
        holder.message.setText(message.getText());
        holder.speaker.setText(message.getFrom());
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class MessageHolder extends RecyclerView.ViewHolder{
        TextView message;
        TextView speaker;
        public MessageHolder(View itemView) {
            super(itemView);
            message = (TextView) itemView.findViewById(R.id.message);
            speaker = (TextView) itemView.findViewById(R.id.speaker);
        }
    }
}
