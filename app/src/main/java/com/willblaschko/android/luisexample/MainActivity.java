package com.willblaschko.android.luisexample;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.RecognizerIntent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.willblaschko.android.microsoftchatbot.ConversationManager;
import com.willblaschko.android.microsoftchatbot.callbacks.ConversationCallback;
import com.willblaschko.android.microsoftchatbot.models.Conversation;
import com.willblaschko.android.microsoftchatbot.models.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private final static int RESULT_SPEECH = 11;

    private static final String TAG = "MainActivity";

    private static final String CHAT_BOT_NAME = "LUIS_BOT"+System.currentTimeMillis();

    RecyclerView recyclerView;
    ChatAdapter adapter;

    String secret;

    Conversation conversation;

    Timer messageCheckTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);

        adapter = new ChatAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        secret = getString(R.string.secret);

        findViewById(R.id.mic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(conversation == null){
                    callback.onError(new IllegalStateException("Conversation has not been initialized yet."));
                    return;
                }

                Intent intent = new Intent(
                        RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

                try {
                    startActivityForResult(intent, RESULT_SPEECH);
                } catch (ActivityNotFoundException a) {
                    Toast.makeText(MainActivity.this,
                            "Oops! Your device doesn't support Speech to Text",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //listen for a result from our get speech to text request
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(text.size() > 0) {
                        conversation.sendMessage(text.get(0), callback);
                    }
                }else{
                    this.finish();
                }
                break;
            }

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startConversation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        restartTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cancelTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopConversation();
    }

    private void startConversation(){
        ConversationManager.getInstance(this)
                .resumeOrCreateConversation(CHAT_BOT_NAME, secret, callback);
    }

    private  void stopConversation(){
        cancelTimer();
    }

    private void restartTimer(){
        if(conversation == null){
            return;
        }
        cancelTimer();

        messageCheckTimer = new Timer();

        messageCheckTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                conversation.getMessages(callback);
                restartTimer();
            }
        }, 1000);
    }

    private void cancelTimer(){
        if(messageCheckTimer != null){
            messageCheckTimer.cancel();
            messageCheckTimer.purge();
            messageCheckTimer = null;
        }
    }



    private final ConversationCallback callback = new ConversationCallback() {
        @Override
        public void conversationAvailable(Conversation conversation) {
            MainActivity.this.conversation = conversation;
            adapter.addItems(conversation.getMessages());
            restartTimer();
        }

        @Override
        public void newMessages(List<Message> messages) {
            adapter.addItems(messages);
            recyclerView.smoothScrollToPosition(recyclerView.getAdapter().getItemCount());
        }

        @Override
        public void onError(final Exception e) {
            new Handler(Looper.getMainLooper()).post(new Runnable(){

                @Override
                public void run() {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Error")
                            .setMessage(e.getMessage())
                            .show();
                }
            });
            e.printStackTrace();
        }
    };

}
