package com.willblaschko.android.microsoftchatbot.callbacks;

import com.willblaschko.android.microsoftchatbot.models.Conversation;
import com.willblaschko.android.microsoftchatbot.models.Message;

import java.util.List;

/**
 * Created by wblaschko on 8/30/16.
 */
public interface ConversationCallback {
    void conversationAvailable(Conversation conversation);
    void newMessages(List<Message> messages);
    void onError(Exception e);
}
