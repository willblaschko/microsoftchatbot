package com.willblaschko.android.microsoftchatbot;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.SugarContext;
import com.willblaschko.android.microsoftchatbot.callbacks.ConversationCallback;
import com.willblaschko.android.microsoftchatbot.models.Conversation;
import com.willblaschko.android.microsoftchatbot.models.Message;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by wblaschko on 8/30/16.
 */
public class ConversationManager {

    private static final String TAG = "ConversationManager";

    public static final MediaType MEDIA_TYPE_MARKDOWN
            = MediaType.parse("text/x-markdown; charset=utf-8");

    private static ConversationManager instance;

    private Context context;

    List<Conversation> conversations;


    private static OkHttpClient client;
    private static Gson gson;


    private ConversationManager(Context context){
        context = context.getApplicationContext();
        SugarContext.init(context);
        conversations = Conversation.listAll(Conversation.class);
    }
    public static ConversationManager getInstance(Context context){
        if(instance == null){
            instance = new ConversationManager(context);
        }
        return instance;
    }

    public void resumeOrCreateConversation(@NotNull String botName, @NotNull String botSecret, @Nullable ConversationCallback callback){

        WeakReference<ConversationCallback> weakCallback = new WeakReference<ConversationCallback>(callback);

        if(TextUtils.isEmpty(botSecret)){
            throw new IllegalArgumentException("Parameter botSecret must not be null or empty.");
        }

        List<Conversation> conversations = Conversation.listAll(Conversation.class);
        Conversation conversation = null;
        for(Conversation con: conversations){
            if(TextUtils.equals(con.getBotName(), botName)){
                conversation = con;
                conversation.setBotName(botName);
                conversation.save();
                break;
            }
        }
        if(conversation == null){
            startConversation(botName, botSecret, weakCallback);
        }else{
            if(weakCallback.get() != null) {
                weakCallback.get().conversationAvailable(conversation);
            }
        }
    }

    private void startConversation(@NotNull final String botName, @NotNull String botSecret, @Nullable final WeakReference<ConversationCallback> weakCallback){

        final Request request = new Request.Builder()
                .url("https://directline.botframework.com/api/conversations")
                .header("Authorization", "BotConnector "+botSecret)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, ""))
                .build();

        getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                if(weakCallback.get() != null){
                    weakCallback.get().onError(e);
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                Log.i(TAG, "Conversation started");

                final String res = response.body().string();
                Conversation conversation = getGson().fromJson(res, Conversation.class);
                conversation.setBotName(botName);
                conversation.save();
                if(weakCallback.get() != null){
                    weakCallback.get().conversationAvailable(conversation);
                }
            }
        });
    }

    public static OkHttpClient getClient(){
        if(client == null){
            client = new OkHttpClient();
        }
        return client;
    }

    public static Gson getGson(){
        if(gson == null){
            gson = new GsonBuilder()
                    .registerTypeAdapter(Message.class, new Message.Serializer())
                    .excludeFieldsWithoutExposeAnnotation()
                    .create();
        }
        return gson;
    }
}
