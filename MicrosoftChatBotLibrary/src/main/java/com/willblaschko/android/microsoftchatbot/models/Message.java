package com.willblaschko.android.microsoftchatbot.models;

import android.text.TextUtils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by willb_000 on 8/25/2016.
 */

public class Message extends SugarRecord {

    @Expose @SerializedName("conversationId")
    String conversation;

    @Expose @SerializedName("created")
    String date;

    @Expose @SerializedName("from")
    String name;

    @Expose @SerializedName("text")
    String message;

    public Message(){

    }

    public String getConversationId() {
        return conversation;
    }

    public String getCreated() {
        return date;
    }

    public String getFrom() {
        return name;
    }

    public String getText() {
        return message;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Message){
            return TextUtils.equals(((Message) obj).getCreated(), getCreated()) &&
                    TextUtils.equals(((Message) obj).getConversationId(), getConversationId())  &&
                    TextUtils.equals(((Message) obj).getFrom(), getFrom())  &&
                    TextUtils.equals(((Message) obj).getText(), getText());
        }
        return false;
    }

    public static class MessageWrapper{
        @Expose
        List<Message> messages = new ArrayList<>();
        @Expose
        String watermark;

        public List<Message> getMessages() {
            return messages;
        }

        public String getWatermark() {
            return watermark;
        }
    }

    public static class Serializer implements JsonSerializer<Message> {
        @Override
        public JsonElement serialize(Message product, Type type, JsonSerializationContext jsc) {
            JsonObject jObj = (JsonObject)new GsonBuilder().create().toJsonTree(product);
            jObj.remove("id");
            return jObj;
        }
    }
}
