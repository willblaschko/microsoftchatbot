package com.willblaschko.android.microsoftchatbot.models;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;
import com.orm.SugarRecord;
import com.willblaschko.android.microsoftchatbot.ConversationManager;
import com.willblaschko.android.microsoftchatbot.callbacks.ConversationCallback;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by willb_000 on 8/25/2016.
 */

public class Conversation extends SugarRecord {

    private static final String TAG = "Conversation";

    @Expose
    String conversationId;
    @Expose
    String token;
    String botName;
    String watermark;

    public Conversation(){

    }
    public Conversation(String conversationId, String token){
        this.conversationId = conversationId;
        this.token = token;
    }

    public void setBotName(String name){
        this.botName = name;
    }

    public String getBotName(){
        return this.botName;
    }

    public void resetWatermark(){
        watermark = "0";
    }

    public List<Message> getMessages(){
        List<Message> messages =  Message.find(Message.class, "conversation = ?", conversationId);
        return messages;
    }

    public String getWatermark(){
        return TextUtils.isEmpty(watermark) ? "0" : watermark;
    }


    public String getConversationId() {
        return conversationId;
    }

    public String getToken() {
        return token;
    }

    public void getMessages(@Nullable ConversationCallback callback){

        Log.i(TAG, "Bot " + botName + " watermark is " + getWatermark());

        final WeakReference<ConversationCallback> weakCallback = new WeakReference<ConversationCallback>(callback);

        final Request request = new Request.Builder()
                .url(String.format(Locale.US, "https://directline.botframework.com/api/conversations/%s/messages?watermark=%s", getConversationId(), getWatermark()))
                .header("Authorization", "BotConnector "+getToken())
                .get()
                .build();

        ConversationManager.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.i(TAG, "Messages received");
                final String res = response.body().string();
                List<Message> messages = parseResponse(res, weakCallback);
                if(weakCallback.get() != null){
                    weakCallback.get().newMessages(messages);
                }
            }
        });
    }

    public void sendMessage(@NotNull final String message, @Nullable final ConversationCallback callback){
        sendMessage(null, message, callback);
    }

    public void sendMessage(@Nullable final String from, @NotNull final String message, @Nullable final ConversationCallback callback){

        final WeakReference<ConversationCallback> weakCallback = new WeakReference<ConversationCallback>(callback);

        RequestBody formBody = new FormBody.Builder()
                .add("text", message)
                .add("from", TextUtils.isEmpty(from) ? "User" : from)
                .add("conversationId", getConversationId())
                .build();

        Request request = new Request.Builder()
                .url(String.format(Locale.US, "https://directline.botframework.com/api/conversations/%s/messages", getConversationId()))
                .header("Authorization", "BotConnector " + getToken())
                .post(formBody)
                .build();

        ConversationManager.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                if(weakCallback.get() != null){
                    weakCallback.get().onError(e);
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.i(TAG, "Message sent: "+message);
                final String res = response.body().string();
                List<Message> messages = parseResponse(res, weakCallback);
                if(weakCallback.get() != null){
                    weakCallback.get().newMessages(messages);
                }
            }
        });

    }

    private List<Message> parseResponse(String response, @Nullable WeakReference<ConversationCallback> weakCallback){
        try {
            Message.MessageWrapper wrapper = ConversationManager.getGson().fromJson(response, Message.MessageWrapper.class);
            if (wrapper == null) {
                return new ArrayList<>();
            }
            watermark = wrapper.getWatermark();
            if(wrapper.getMessages() != null) {
                Log.i(TAG, "Messages added: " + wrapper.getMessages().size());
                Message.saveInTx(wrapper.getMessages());
            }
            Conversation.this.save();
            return wrapper.getMessages();
        }catch (JsonSyntaxException e){
            if(weakCallback.get() != null){
                weakCallback.get().onError(e);
            }
        }
        return new ArrayList<>();
    }
}
